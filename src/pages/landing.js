import React from "react";
import "./landing.css";
import Computer from "./../images/svgs/computer.svg";
import Instagram from "./../images/svgs/instagram.svg";
import Dribble from "./../images/svgs/dribbble.svg";
import Linkedin from "./../images/svgs/linkedin.svg";
import Twitter from "./../images/svgs/twitter.svg";
import constants from "../utils/constants";

const Landing = () => {
  function onWhatsappClick() {
    window.open(constants.WHATSAPP_LINK, "_blank");
  }
  function onCaseStudyClick(){
    window.open('/case-study',"_self")
  }
  function onDribble(){
    window.open('https://dribbble.com/inspired_monster')
  }

  function onTwitter(){
    window.open("https://twitter.com/inspired_monstr")
  }
  function onLinkedIn(){
    window.open("https://www.linkedin.com/in/arunsajeev/?originalSubdomain=in")
  }

  function onInstagram(){
    window.open("https://www.instagram.com/inspired_monster/")
  }
  return (
    <div>
      <div className="landing-main">
        <div className="landing-computer">
          <div className="computer-weight"></div>
          <Computer className="computer" />
        </div>
        <div className="landing-message">
          <div className="message-weight" />
          <p className="landing-title">
            I am <b className="landing-name">Arun Sajeev</b>, a
            multidisciplinary designer based in Kochi, India, with over 5 years
            of experience in delivering world-class UI/UX, interaction and
            visual designs.
          </p>
          <div className="landing-contacts">
            <Dribble className="landing-social"  onClick={onDribble}/>
            <Twitter className="landing-social"  onClick={onTwitter}/>
            <Linkedin className="landing-social"  onClick={onLinkedIn}/>
            <Instagram className="landing-social"  onClick={onInstagram}/>
          </div>
          <div className="landing-actions">
            <div className="landing-button" onClick={onWhatsappClick}>
              <img
                src={'/whatsapp.png'}
                className="landing-button-icon"
              />
              <div>Talk to me</div>
            </div>

            <div className="btn-case-study" onClick={onCaseStudyClick}>
                <p>Case Study</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landing;
