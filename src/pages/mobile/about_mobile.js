import React from 'react'
import './about_mobile.css'


import Instagram from './../../images/svgs/instagram.svg'
import Dribble from './../../images/svgs/dribbble.svg'
import Linkedin from './../../images/svgs/linkedin.svg'
import Twitter from './../../images/svgs/twitter.svg'
import constants from './../../utils/constants'

const AboutMobile=()=>{

    function onDownloadResumeClick(){
        window.open(constants.RESUME_LINK, "_blank")
    }


      function onDribble(){
        window.open('https://dribbble.com/inspired_monster')
      }
    
      function onTwitter(){
        window.open("https://twitter.com/inspired_monstr")
      }
      function onLinkedIn(){
        window.open("https://www.linkedin.com/in/arunsajeev/?originalSubdomain=in")
      }
    
      function onInstagram(){
        window.open("https://www.instagram.com/inspired_monster/")
      }

    return   <div className="about-mobile"> 
    <div className="about-mobile-root">
        <div className="about-mobile-image-root">
        <img src={constants.ARUN_IMAGE_MOBILE} className="about-mobile-image"/>
     
       <div className="about-mobile-head">
       <h1>Arun Sajeev</h1>
        <div className="about-mobile-contacts">
            <Dribble className="landing-social"  onClick={onDribble}/>
            <Twitter className="landing-social"   onClick={onTwitter}/>
            <Linkedin className="landing-social"  onClick={onLinkedIn}/>
            <Instagram className="landing-social" onClick={onInstagram} />
        </div>
       </div>
        </div>
   

    <div className="about-mobile-details">
       
       <div className="about-mobile-description">
       <p>
        I am a multidisciplinary designer obsessed with solving problems through rendering user needs to design solutions. I focus on User Experience and Interaction Design for digital products in bridging the gaps between users and their goals, all the while balancing business values and good experiences.</p>
        <br/>
        <p>
        Having worked with India2 Startups and communities, I am skilled at following agile, communicating stakeholder needs, and cross-functional collaboration. I am now the Head of Product Design at Entri.
        </p>
       </div>
       <div className="about-mobile-button" onClick={onDownloadResumeClick}>
           Download Resume
       </div>
      
    </div>
    </div>

    </div>
}

export default AboutMobile