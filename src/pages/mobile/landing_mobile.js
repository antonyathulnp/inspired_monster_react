import React from 'react'
import './landing_mobile.css'

import Instagram from "./../../images/svgs/instagram.svg";
import Dribble from "./../../images/svgs/dribbble.svg";
import Linkedin from "./../../images/svgs/linkedin.svg";
import Twitter from "./../../images/svgs/twitter.svg";
import Computer from "./../../images/svgs/computer.svg";
import constants from "../../utils/constants";

const LandingMobile=()=>{
  function onWhatsappClick() {
    window.open(constants.WHATSAPP_LINK, "_blank");
  }
    return (
      <div className="landing-mobile-main">
          <div className="landing-mobile-message">
          <p className="landing-mobile-title">
            I am <b className="landing-mobile-name">Arun Sajeev</b>, a
            multidisciplinary designer based in Kochi, India, with over 5 years
            of experience in delivering world-class UI/UX, interaction and
            visual designs.
          </p>
          </div>
          
          <div className="landing-mobile-contacts">
            <Dribble className="landing-mobile-social" />
            <Twitter className="landing-mobile-social" />
            <Linkedin className="landing-mobile-social" />
            <Instagram className="landing-mobile-social" />
          </div>
        <div className="landing-mobile-weight"></div>
        <div className="landing-mobile-computer">
          <Computer className="landing-mobile-computer-image"/>
        </div>
        <div className="landing-mobile-grey"></div>

        <div className="landing-mobile-button" onClick={onWhatsappClick}>
              <img
                src={'/whatsapp.png'}
                className="landing-mobile-button-icon"
              />
              <div>Talk to me</div>
        </div>
      </div>
    )
}


export default LandingMobile