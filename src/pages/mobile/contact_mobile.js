import React from 'react'
import './contact_mobile.css'
import EditText from '../../widgets/edittext/EditText'

const ContactMobile=()=>{

    let [type,setType]=React.useState(-1)

    function getClassFromType(index){
        if(type===index){
            return "project-type-mobile-active"
        }
        return "project-mobile-type"
    }
   
    return <div className="contact-mobile-main">
       <div className="contact-mobile-title">Let's Chat!</div>
       <div className="contact-mobile-desc">
       For faster reply text or call me  (📲 
                <a href="https://api.whatsapp.com/send?phone=+919946701501" target="_blank">9946701501</a>
                ) For easy connect, find me on instagram ️or you can also email me at <a href="mailto: arun14949@gmail.com" 
                target="_blank">arun14949@gmail.com</a>
       </div>
       <div className="contact-mobile-have-project">Have a project to talk about?</div>
            <p className="form-input-field-mobile">
            <EditText label="Name" type="input"/>
            </p>

            <p className="form-input-field-mobile">
            <EditText label="Email or Phone" type="input"/>
            </p>
            <div className="form-project-type-mobile">What kind of project is this?</div>
            <div className="project-types-mobile">
                <div className={getClassFromType(0)} onClick={()=>{setType(0)}}>Website</div>
                <div className={getClassFromType(1)} onClick={()=>{setType(1)}}>Branding</div>
                <div className={getClassFromType(2)} onClick={()=>{setType(2)}}>App/Software</div>
                <div className={getClassFromType(3)} onClick={()=>{setType(3)}}>Graphic Design</div>
                <div className={getClassFromType(4)} onClick={()=>{setType(4)}}>Consulting</div>
                <div className={getClassFromType(5)} onClick={()=>{setType(5)}}>Others</div>
                
            </div>

            <p className="form-input-field-mobile">
            <EditText label="Describe your project*"/>
            </p>
            <div className="btn-submit-mobile">Submit Project</div>
    </div>
}

export default ContactMobile