import React from 'react'
import a from '../actions'
import Layout from '../components/layout'
import './case_study.css'
import SEO from '../components/seo'



const CaseStudy=()=>{

    React.useEffect(async ()=>{
        await a.parseMediumProfile()
    })
    return(
        <Layout position={3}>
            <SEO title="Case Study"/>
            <div></div>
        </Layout>
    )
}

export default CaseStudy