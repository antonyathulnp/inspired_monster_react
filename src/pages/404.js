import * as React from "react"

import Layout from "./../components/layout";
import SEO from './../components/seo'


const NotFoundPage = () => {
  return (
    <Layout position={-1}>
      <SEO title="Not Found"/>
      <title>Inspired Monster | 404 Not found</title>
      <h1>Page not found</h1>
    </Layout>
  )
}

export default NotFoundPage
