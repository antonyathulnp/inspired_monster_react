import React from 'react'
import './about.css'

import Instagram from './../images/svgs/instagram.svg'
import Dribble from './../images/svgs/dribbble.svg'
import Linkedin from './../images/svgs/linkedin.svg'
import Twitter from './../images/svgs/twitter.svg'
import Layout from "./../components/layout";
import constants from './../utils/constants'
import SEO from './../components/seo'
import AboutMobile from './mobile/about_mobile'

const About=()=>{

    function onDownloadResumeClick(){
        window.open(constants.RESUME_LINK, "_blank")
    }


      function onDribble(){
        window.open('https://dribbble.com/inspired_monster')
      }
    
      function onTwitter(){
        window.open("https://twitter.com/inspired_monstr")
      }
      function onLinkedIn(){
        window.open("https://www.linkedin.com/in/arunsajeev/?originalSubdomain=in")
      }
    
      function onInstagram(){
        window.open("https://www.instagram.com/inspired_monster/")
      }

    return(
       <Layout position={1}>
           <SEO title="About Me"/>
           <AboutMobile/>
            <div className="about-landscape"> 
            <div className="about-root">
                <div className="about-image-root">
                <img src={constants.ARUN_IMAGE_DESKTOP} className="about-image"/>
                <div className="about-button" onClick={onDownloadResumeClick}>
                   Download Resume
               </div>
                </div>
           

            <div className="about-details">
                <h1>Arun Sajeev</h1>
                <div className="about-contacts">
                    <Dribble className="landing-social"  onClick={onDribble}/>
                    <Twitter className="landing-social"   onClick={onTwitter}/>
                    <Linkedin className="landing-social"  onClick={onLinkedIn}/>
                    <Instagram className="landing-social" onClick={onInstagram} />
                </div>
               <div className="about-description">
               <p>
                I am a multidisciplinary designer obsessed with solving problems through rendering user needs to design solutions. I focus on User Experience and Interaction Design for digital products in bridging the gaps between users and their goals, all the while balancing business values and good experiences.</p>
                <br/>
                <p>
                Having worked with India2 Startups and communities, I am skilled at following agile, communicating stakeholder needs, and cross-functional collaboration. I am now the Head of Product Design at Entri.
                </p>
               </div>

              
            </div>
            </div>

            </div>
       </Layout>
    )
}

export default About