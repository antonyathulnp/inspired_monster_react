import React from 'react'
import './contact.css'
import Layout from '../components/layout'
import EditText from './../widgets/edittext/EditText'
import constants from '../utils/constants'
import SEO from './../components/seo'
import ContactMobile from './mobile/contact_mobile'

const Contact=()=>{

    let [type,setType]=React.useState(-1)

    function getClassFromType(index){
        if(type===index){
            return "project-type-active"
        }
        return "project-type"
    }


    return <Layout position={4}>
        <SEO title="Contact"/>
        <ContactMobile/>
        <div className="contact-root">
            <div className="contact-stat">
                <div className="contact-image-root">
                    <img className="contact-image"/>
                </div>
                <div className="ready-chat-title">Let's Chat!</div>
                <div className="ready-chat-desc">
                For faster reply text or call me  (📲 
                <a href="https://api.whatsapp.com/send?phone=+919946701501" target="_blank">9946701501</a>
                ) For easy connect, find me on instagram ️or you can also email me at <a href="mailto: arun14949@gmail.com" 
                target="_blank">arun14949@gmail.com</a>
                </div>
            </div>
            <div className="contact-form"> 
            <div className="form-title">Have a project to talk about? </div>

            <p className="form-input-field">
            <EditText label="Name" type="input"/>
            </p>

            <p className="form-input-field">
            <EditText label="Email or Phone" type="input"/>
            </p>
            <div className="form-project-type">What kind of project is this?</div>
            <div className="project-types">
                <div className={getClassFromType(0)} onClick={()=>{setType(0)}}>Website</div>
                <div className={getClassFromType(1)} onClick={()=>{setType(1)}}>Branding</div>
                <div className={getClassFromType(2)} onClick={()=>{setType(2)}}>App/Software</div>
                <div className={getClassFromType(3)} onClick={()=>{setType(3)}}>Graphic Design</div>
                <div className={getClassFromType(4)} onClick={()=>{setType(4)}}>Consulting</div>
                <div className={getClassFromType(5)} onClick={()=>{setType(5)}}>Others</div>
                
            </div>
            <p className="form-input-field">
            <EditText label="Describe your project*"/>
            </p>
            <div className="btn-submit">Submit Project</div>
            </div>
    </div>
    </Layout>
}

export default Contact