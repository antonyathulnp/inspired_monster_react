import * as React from "react"
import { useState, useEffect } from 'react';
import './index.css'
import Layout from '../components/layout'
import SEO from "../components/seo"
import Landing from './landing'
import LandingMobile from './mobile/landing_mobile'


const IndexPage = () => {

  return (
    <Layout position={0}>
       <SEO title="Home" />
       <Landing class="landing-desk"/>
       <LandingMobile  class="landing-mobile"/>
       
    </Layout>

  )
}

export default IndexPage
