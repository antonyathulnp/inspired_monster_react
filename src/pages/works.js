import React, { useEffect, useState } from 'react'
import './works.css'
import actions from './../actions/index'
import Layout from '../components/layout'
import SEO from './../components/seo'

const Works=()=>{
    let [data,setData]=useState([])

    useEffect(async()=>{
        async function fetch (){
            let data=await actions.getPortfolios()
            setData(data)
        }
        fetch()
    },[])

    function getGridClassName(position){
        if(position===0 || position===7
            || position===14 || position===21 || position===28){
            return "grid-item span-0"
        }
        if(position===1){
            return "grid-item span-2"
        }
        if(position%2===0){
            return "grid-item span-2"
        }
        if(position%3===0){
            return "grid-item span-3"
        }
        return "grid-item span-1"
    }

    return(
        <Layout position={2}>
            <SEO title="Works"/>
        
        <div class="grid-layout">
            {
                data.map((item,index)=>{
                    return <img src={item.images.two_x} className={getGridClassName(index)}></img>
                   //return <div className={getGridClassName(index)}> <span>{index} => {getGridClassName(index)}</span> </div>
                 })
            
            }   
    
        </div>
        </Layout>
    )
}

export default Works