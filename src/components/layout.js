import * as React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import {useState,useEffect} from 'react'
import BottomMenu from './../widgets/bottom_menu/bottom_menu'
import Header from "./header"
import Footer from './../widgets/footer/footer'


const Layout = ({ children,position }) => {

  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)

  return (
    <div className="layout-main">
      <Header  menuLinks={data.site.siteMetadata.menuLinks} siteTitle={data.site.siteMetadata.title} child={position} />
        <main>{children}</main>
        <BottomMenu child={position}/> 
         <Footer/>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
