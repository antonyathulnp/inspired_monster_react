module.exports={
    BASE_URL:"https://us-central1-inspiredmonster-136b1.cloudfunctions.net/api/",
    ARUN_IMAGE_DESKTOP:"https://firebasestorage.googleapis.com/v0/b/inspiredmonster-136b1.appspot.com/o/arun_desktop.png?alt=media&token=f9727857-949b-4ab9-9728-2cef823ae72e",
    MEDIUM_PROFILE_FEED:"https://medium.com/feed/@inspired_monster",
    WHATSAPP_LINK:"https://api.whatsapp.com/send?phone=+919946701501",
    RESUME_LINK:"https://drive.google.com/file/d/1zVnBR-cMfm2J4qQM62iRChynHD43kPu8/view?usp=sharing",
    ARUN_IMAGE_MOBILE:"https://firebasestorage.googleapis.com/v0/b/inspiredmonster-136b1.appspot.com/o/arun_mobile.png?alt=media&token=a2d6ee39-7c61-4734-86a6-4e31306785d8"
}