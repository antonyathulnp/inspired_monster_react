import React from 'react'
import './bottom_menu.css'
import HomeInactive from './../../images/svgs/menu_home_inactive.svg'
import CaseStudyInactive from './../../images/svgs/menu_case_study_inactive.svg'
import ContactInactive from './../../images/svgs/menu_contact_inactive.svg'
import MonsterInactive from './../../images/menu_monster_inactive.png'
import { navigate } from 'gatsby'

const BottomMenu=({child})=>{

    function getTitleClass(index){
        return index===child ? "menu-title-active" : "menu-title"
    }
    function onMenuItemClick(index){
        if(index===0){
            navigate('/')
        }
        if(index===1){
            navigate('/case-study')
        }
        if(index===2){
            navigate('/contact')
        }

        if(index===3){
            navigate('/about')
        }
    }

    return (
        <div className="bottom-menu-main">
            <div className="menu-item" onClick={()=>onMenuItemClick(0)}>
                <HomeInactive/>
                <div className={getTitleClass(0)}>Home</div>
            </div>

            <div className="menu-item" onClick={()=>onMenuItemClick(1)}>
                <CaseStudyInactive/>
                <div className={getTitleClass(3)}>Case Study</div>
            </div>


            <div className="menu-item" onClick={()=>onMenuItemClick(2)}>
                <ContactInactive/>
                <div className={getTitleClass(4)}>Contact</div>
            </div>

            <div className="menu-item" onClick={()=>onMenuItemClick(3)}>
                <img src={MonsterInactive}/>
                <div className={getTitleClass(1)}>About Me</div>
            </div>

    </div>
    )
}

export default BottomMenu