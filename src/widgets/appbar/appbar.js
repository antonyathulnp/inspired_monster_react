import React from "react";
import "./appbar.css";
import Icon from './../../images/svgs/logo.svg'
import { useState, useEffect } from 'react';
import { Link } from "gatsby"


const AppBar = ({child}) => {
  function getClassNameForLink(index){
    if(child===index || (child===undefined && index===0)){
      return "menu-active"
    }else{
      
      return null
    }
  }

  function goHome(){
    window.open("/","_self")
  }
  return (
    <div className="appbar-root">
      <Icon onClick={goHome} className="appbar-logo"/>
    <div className="appbar-options">
      <ul className="appbar-menu-list">
        <li>
          <Link to="/" className={getClassNameForLink(0)}>
            Home
          </Link>
        </li>
        <li>
          <Link to="/about" className={getClassNameForLink(1)}>About Me</Link>
        </li>
        <li>
          <Link to="/works" className={getClassNameForLink(2)}>Works</Link>
        </li>
        <li>
          <Link to="/case-study" className={getClassNameForLink(3)}>Case Study</Link>
        </li>

        <li>
          <Link to="/contact" className={getClassNameForLink(4)}>Contact</Link>
        </li>
      </ul>
      </div>

    </div>
  );
};

export default AppBar;
