import React from "react";
import "./footer.css";

const DARK_CLASS = "dark";
const DARK_KEY="is_dark_enabled"



const Footer = () => {

  function isDarkMode(){
    // let isDark=(localStorage.getItem(DARK_KEY)==='true')
    // return isDark
    return true
  }

  const [isDark, setIsDark] = React.useState(isDarkMode());

  React.useEffect(() => {
    // let contains=document.documentElement.classList.contains(DARK_CLASS)
    // if (!isDark) {
    //   console.log(`removing dark class ${contains}`)
    //   document.documentElement.classList.remove(DARK_CLASS);
    // } else {
    //   if(contains){
    //     document.documentElement.classList.remove(DARK_CLASS);
    //   }else{
    //     document.documentElement.classList.add(DARK_CLASS);
    //   }
     
    // }
  }, [isDark]);

  return (
    <div className="footer-main">
      <div className="footer-year">
        {`© ${new Date().getFullYear()} Inspired Monster  ·  credits`}
      </div>
      {/* <div className="footer-day-icon">
        <DayIcon onClick={()=>{
            setIsDark(!isDark)
            localStorage.setItem(DARK_KEY,isDark)
        }}/>
      </div> */}
    </div>
  );
};

export default Footer;
