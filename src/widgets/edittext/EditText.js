import React from 'react'
import './edittext.css'

const EditText=({label,type})=>{

    return    <div className="group">   
    {
      type === "input" ?  <input type="text" required/> : <textarea type="text" required></textarea>
    }   
   
    <label>{label}</label>
  </div>

  
}


export default EditText