import axios from 'axios'
import constants from './../utils/constants'


let a={
    getPortfolios: async()=>{
        try{
            var result=await axios.get(constants.BASE_URL+"portfolios")
            return result.data
        }catch(e){
            console.log(e)
        }
    },

    parseMediumProfile:async()=>{
        try{
            
            var result=await axios.get(constants.MEDIUM_PROFILE_FEED)
            console.log(result)
        }catch(e){
            console.log(e)
        }
    }
}

export default a
