module.exports = {
  siteMetadata: {
    title: "Inspired Monster",
    description: `Portfolio site of Arun Sajeev a.k.a Inspired Monster`,
    author: `@athulantonynp`,
    menuLinks:[
      {
        name:'home',
        link:'/'
      },
      {
        name:'about',
        link:'/about'
      },
      {
        name:'portfolio',
        link:'/portfolio'
      }
    ]
  },
  plugins: [
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /svgs/
        }
      }
    },
    {
          resolve: `gatsby-plugin-manifest`,
          options: {
            name: "Inspired Monster",
            short_name: "IM",
            start_url: "/",
            background_color: "#2A2A2A",
            theme_color: "#2A2A2A",
            // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
            // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
            display: "standalone",
            icon: "src/images/favicon.png", // This path is relative to the root of the site.
            // An optional attribute which provides support for CORS check.
            // If you do not provide a crossOrigin option, it will skip CORS for manifest.
            // Any invalid keyword or empty string defaults to `anonymous`
            crossOrigin: `anonymous`,
          },
        },
  ],
};
